-- Proyecto SQL Basico --
CREATE DATABASE Jardineria;
USE Jardineria;

-- Sección de creación de tablas -- 
CREATE TABLE Cliente(
    rfc VARCHAR(13) NOT NULL PRIMARY KEY,
    nombre_c VARCHAR(255) NOT NULL,
    domicilio VARCHAR(255) NOT NULL,
    tipo_jardin VARCHAR(255) NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    email VARCHAR(55)
);

CREATE TABLE Contrato(
    id_contrato BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    rfc VARCHAR(13) NOT NULL,
    duracion VARCHAR(20) NOT NULL,
    tarifa FLOAT NOT NULL,

    FOREIGN KEY(rfc) REFERENCES Cliente(rfc)
);

CREATE TABLE Jardin(
    id_jardin BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    id_contrato BIGINT NOT NULL,
    nombre_j VARCHAR(255) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    tipo VARCHAR(255) NOT NULL,
    extension FLOAT NOT NULL, 

    FOREIGN KEY(id_contrato) REFERENCES Contrato(id_contrato)
);

CREATE TABLE Plantas(
	id_planta BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	tipo_flor VARCHAR(55),
	imagen BLOB,
	aspecto VARCHAR(255),
	nombre_p VARCHAR(55) NOT NULL,
	tipo_hoja VARCHAR(55),
	temporada_plantacion VARCHAR(255),
	temporada_floracion VARCHAR(255),
	origen VARCHAR(255),
	familia VARCHAR(255)
);

CREATE TABLE Contiene(
	id_planta BIGINT NOT NULL,
	id_jardin BIGINT NOT NULL,
	fecha_plantacion DATE NOT NULL,
	cant_plantas INT NOT NULL,
    primary key(id_planta, id_jardin),
	foreign key (id_planta) references Plantas(id_planta),
	foreign key (id_jardin) references Jardin(id_jardin)
);

CREATE TABLE Trabajador( 
    id_trabajador BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    sueldo FLOAT NOT NULL,
    nombre_t CHAR(60) NOT NULL,
    horas_extra int NOT NULL
);
-- actualizado--
CREATE TABLE Tareas(
	id_tarea BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	id_trabajador BIGINT,
	id_contrato BIGINT NOT NULL,
	horas INT NOT NULL,
	descripcion_t VARCHAR(255),
	foreign key (id_trabajador) references Trabajador(id_trabajador),
	foreign key (id_contrato) references Contrato(id_contrato)
);

CREATE TABLE Inventario( 
    id_inventario BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,        
    descripcion_i CHAR(255),    
    existencia INT NOT NULL,
    nombre_i CHAR(55) NOT NULL
);

CREATE TABLE Utiliza 
(
	id_tarea BIGINT NOT NULL,
	id_inventario BIGINT NOT NULL,
	cantidad int NOT NULL,
	primary key(id_tarea, id_inventario),
	foreign key (id_tarea) references Tareas(id_tarea),
	foreign key (id_inventario) references Inventario(id_inventario)	
);


-- Inserción de clientes --
INSERT INTO Cliente VALUES('CUPU800825569', 'José Rofríguez', 'Provedencia #65', 'Jardín pequeño', '3111239840', 'jose_hola@gmail.com');
INSERT INTO Cliente(rfc, nombre_c, domicilio, tipo_jardin, telefono) VALUES('PARM950928PP9', 'Mario Ignacio Panduro Ramos', 'Constituyentes #99', 'Jardín grande', '3291254389');
INSERT INTO Cliente VALUES('ZAPM840906E53', 'Maria M. Zavala Patricio', 'Federico del Toro #50', 'Jardín mediano', '2340982345', 'maria@hotmail.com');

-- Inserción de contratos --
INSERT INTO Contrato(rfc, duracion, tarifa) VALUES('CUPU800825569', '20 días', 3599.99);
INSERT INTO Contrato(rfc, duracion, tarifa) VALUES('ZAPM840906E53', '50 días', 40080.35);
INSERT INTO Contrato(rfc, duracion, tarifa) VALUES('PARM950928PP9', '7 días', 999.99);

-- Inserción de trabajadores --
INSERT INTO Trabajador(sueldo,nombre_t,horas_extra) VALUES(3700.5,"Juan Camacho",0);
INSERT INTO Trabajador(sueldo,nombre_t,horas_extra) VALUES(1500.5,"Jose Camacho",0);
INSERT INTO Trabajador(sueldo,nombre_t,horas_extra) VALUES(3700.5,"Roberto Camacho",0);

-- Inserción de inventario --
INSERT INTO Inventario(existencia,nombre_i) VALUES(5,"Talacho");
INSERT INTO Inventario(existencia,nombre_i) VALUES(3,"Tijeras para podar");
INSERT INTO Inventario(descripcion_i,existencia,nombre_i) VALUES("Abono organico de EcoFEMEX",3,"Abono");

-- Inserción de Jardín --
INSERT INTO Jardin(id_contrato, nombre_j, direccion, tipo, extension) VALUES(3,"Las jarras","Colón #50","Rosaleda",10.5);
INSERT INTO Jardin(id_contrato, nombre_j, direccion, tipo, extension) VALUES(1,"Chamizal","Reforma #450","Alameda",20);
INSERT INTO Jardin(id_contrato, nombre_j, direccion, tipo, extension) VALUES(2,"Rocas","San Pedro #20","Huerto",35);

-- Inserción de Plantas --
INSERT INTO Plantas(tipo_flor, nombre_p, origen, familia) VALUES("Magnoliophyta","Tulipán", "Tulipeae" ,"Liliacea");
INSERT INTO Plantas(tipo_flor, nombre_p, origen, familia) VALUES("Helianthus annuus","Girasol", "Heliantheae" ,"Asteraceae");
INSERT INTO Plantas(tipo_flor, nombre_p, origen, familia) VALUES("Magnoliophyta","Rosa", "Roseae" ,"Rosaceae");

-- Inserción contiene --
INSERT INTO Contiene(id_planta, id_jardin, fecha_plantacion, cant_plantas) VALUES(2, 1, '2021-02-15', 5);
INSERT INTO Contiene(id_planta, id_jardin, fecha_plantacion, cant_plantas) VALUES(1, 2, '2021-01-15', 2);
INSERT INTO Contiene(id_planta, id_jardin, fecha_plantacion, cant_plantas) VALUES(3, 3, '2021-02-14', 10);

-- Inserción de Tareas --
INSERT INTO Tareas(id_trabajador,id_contrato,horas,descripcion_t) VALUES(1,1,3,"Plantar orquideas en zona abierta del jardin");
INSERT INTO Tareas(id_trabajador,id_contrato,horas,descripcion_t) VALUES(2,1,1,"Regar orquideas");
INSERT INTO Tareas(id_contrato,horas,descripcion_t) VALUES(1,2,"Regar arboles");

-- Inserción de Tareas --
INSERT INTO Utiliza VALUES(1,1,2);
INSERT INTO Utiliza VALUES(1,2,1);
INSERT INTO Utiliza VALUES(2,3,3);


-- Eliminación de contiene --
DELETE FROM Contiene WHERE id_planta = 2 AND id_jardin = 1;

-- Eliminación de utiliza --
DELETE FROM  Utiliza WHERE id_tarea = 1 AND id_inventario = 1;

-- Modificación de Planta --
UPDATE Plantas 
SET aspecto = 'Rosa', tipo_hoja = 'Puntiaguda'
WHERE id_planta = 3; 

-- Modificación de Trabajador  --
UPDATE Trabajador
SET nombre_t = 'Magnolio Flores', horas_extra = 3
WHERE id_trabajador = 2;

-- Modificación de Jardín --
UPDATE Jardin
SET nombre_c = 'El escondite'
WHERE id_jardin = 2;
SELECT * FROM Jardin;
