package src;

import java.util.ArrayList;

/**
 * @author venom
 * @version 1.0
 */
public class SSTF extends Planner {

    public SSTF(ArrayList<Integer> cylinders, int initialCyl) {
        super(cylinders, initialCyl);

        addCylinder(initialCyl);
        sortCylinders();
    }

    void calculate() {
        ArrayList<Integer> cylinders = getCylinders();
        boolean flag = true;
        int currentCyl = getInitialCyl(),
                left = 0,
                right = 0,
                pointer = cylinders.indexOf(getInitialCyl()),
                displacement,
                waitTime = 0,
                sumWaitingTime = 0,
                oldCylinder = currentCyl;

        // Print
        System.out.printf("%-10s | %-10s | %-10s | %-10s\n", "Cilindro actual", 
                "Cilindro solicitado", "Tiempo de espera", "Desplazamiento");

        for (int x = 0; x < cylinders.size() - 1; x++) {
            if (pointer != 0 && flag) {
                if (Math.abs(currentCyl - cylinders.get(pointer - 1 - right))
                        < Math.abs(currentCyl - cylinders.get(pointer + 1 + left))) {
                    currentCyl = cylinders.get(pointer - 1 - right);
                    left += right + 1;
                    pointer -= 1 + right;
                    right = 0;
                } else {
                    currentCyl = cylinders.get(pointer + 1 + left);
                    right += left + 1;
                    pointer += 1 + left;
                    left = 0;

                }
            } else {
                pointer += left + 1;
                currentCyl = cylinders.get(pointer);
                flag = false;

                left = 0;
            }

            displacement = getDisplacement(oldCylinder, currentCyl);
            System.out.printf("%-15s | %-19s | %-16s | %-15s\n", oldCylinder, 
                    currentCyl, waitTime, displacement);

            sumWaitingTime += waitTime;
            waitTime += displacement;
            oldCylinder = currentCyl;
            
            // Exists not matched cylinders after the last list element?
            if (pointer == cylinders.size() - 1 && right != 0) {
                pointer -= right;
                currentCyl = cylinders.get(pointer - 1);
                right = 0;
            }
        }

        removeCylinder(getInitialCyl()); // Remove the initial cylinder from the cylinders list
        System.out.println("\nResultados finales\nDesplazamiento: " + waitTime
                + "\nTPE: " + avgWaitingTime(sumWaitingTime));
    }
}
