
package src;

import java.util.ArrayList;

/**
 *
 * @author marti
 */
public class SCAN extends Planner{

    public SCAN(ArrayList<Integer> cylinders, int initialCyl) {
        super(cylinders, initialCyl);
    }

    @Override
    void calculate() {        
        int currentCyl = getInitialCyl();
        int displacement, waitTime = 0, sumWaitingTime = 0;
        sortCylinders();
        ArrayList <Integer> cylinders = getCylinders();
        boolean up;
        int start, n, auxCyl; //n helps us indicate in which stop the "elevator" currently is
        
        up = false;
        n = start = firstClosestCyl(up);        
        
        System.out.printf("%-10s | %-10s | %-10s | %-10s\n", "Cilindro actual"
                , "Cilindro solicitad0", "Tiempo de espera", "Desplazamiento");
               
        for(int i = 0; i < cylinders.size(); i++) {
            //if n has rreach a limit change its direction 
            if(n<0){                                
                n = start;
                up = !up;
                //checks if the cylinder already has a 0 cylinder
                auxCyl = cylinders.get(0);
                if(auxCyl == 0){
                    n++;
                    continue;
                }else{   
                    auxCyl = 0;
                    i --;                   
                }
                
            }else if(n>=cylinders.size()){
                n = start;
                up = !up;
                //checks if the cylinder already has a 199 cylinder
                auxCyl = cylinders.get(cylinders.size()-1);
                if(auxCyl == 199){
                    n--;
                    continue;
                }else{   
                    auxCyl = 199;
                    i --;                  
                }                
            }else 
                auxCyl = cylinders.get(n);
            
            displacement = getDisplacement(currentCyl, auxCyl);
            
            System.out.printf("%-15s | %-19s | %-16s | %-15s\n", currentCyl, auxCyl, waitTime, displacement);

            sumWaitingTime += waitTime;
            waitTime += displacement;
            currentCyl = auxCyl;
            
            if(up)
                n++;
            else
                n--;
        }

        System.out.println("\nResultados finales\nDesplazamiento: " + waitTime +
                "\nTPE: " + avgWaitingTime(sumWaitingTime)); 
    }
    
}
