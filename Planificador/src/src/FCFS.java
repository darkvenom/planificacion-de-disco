package src;

import java.util.ArrayList;

/**
 * This class calculate the FCFS algorithm
 * @author Carlos Acuña
 * @version 1.1
 */
public class FCFS extends Planner {
    public FCFS(ArrayList<Integer> data, int initialCylinder) {
        super(data, initialCylinder);
    }
    
    @Override
    void calculate() {
        int currentCyl = getInitialCyl();
        int displacement, waitTime = 0, sumWaitingTime = 0;
        ArrayList <Integer> cylinders = getCylinders();

        System.out.printf("%-10s | %-10s | %-10s | %-10s\n", "Cilindro actual", "Cilindro solicitado",
                "Tiempo de espera", "Desplazamiento");

        
        for(int x = 0; x < cylinders.size(); x++) {
            displacement = getDisplacement(currentCyl, cylinders.get(x));

            System.out.printf("%-15s | %-19s | %-16s | %-15s\n", currentCyl, cylinders.get(x), waitTime, displacement);

            sumWaitingTime += waitTime;
            waitTime += displacement;
            currentCyl = cylinders.get(x);
        }

        System.out.println("\nResultados finales\nDesplazamiento: " + waitTime +
                "\nTPE: " + avgWaitingTime(sumWaitingTime));
    }
}
