package src;

import java.util.ArrayList;

/**
 * This class calculate the C-LOOK algorithm
 * @author mickie
 * @version 1.1
 */
public class CLOOK extends Planner {
   
   public CLOOK(ArrayList<Integer> cylinders, int initialCyl) {
      super(cylinders, initialCyl);
   }
   
   @Override
   void calculate() {
      int currentCyl = getInitialCyl();
      int displacement;
      int waitTime = 0;
      int sumWaitingTime = 0;
      boolean up = false;
      
      ArrayList<Integer> cylinders = getCylinders();
      
      sortCylinders();
      int start = firstClosestCyl(up);
      int auxCyl;
      int n = firstClosestCyl(up);
      int count = 0;
      
      System.out.printf("%-10s | %-10s | %-10s | %-10s\n",
                       "Cilindro actual",
                       "Cilindro solicitado",
                       "Tiempo de espera", 
                       "Desplazamiento");
      
      for (int i = 0; i < cylinders.size(); i++) {
         if (n<0) {
            auxCyl = cylinders.get(0);
            i --;
            up = !up;
            n = start;
            count++; // when is the first element of the array
         } else if (count == 1) {
             // start from the last element of the array
             auxCyl = cylinders.get(cylinders.size()-1);
             up = false;
             count = 0;
             n = cylinders.size() -1;
         }else
            auxCyl = cylinders.get(n);
         
         displacement = getDisplacement(currentCyl, auxCyl);
         
         if (displacement != 0) {
            System.out.printf("%-15s | %-19s | %-16s | %-15s\n",
                             currentCyl, 
                             auxCyl, 
                             waitTime, 
                             displacement);
         
            sumWaitingTime += waitTime;
            waitTime += displacement;
            currentCyl = auxCyl;
         }
         
         if (up)
            n++;
         else
            n--;
         
      }
      
      System.out.println("\nResultados finales\nDesplazamiento: " + waitTime +
             "\nTPE: " + avgWaitingTime(sumWaitingTime));
   }
}
