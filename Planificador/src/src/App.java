package src;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Main class
 * @author Venom
 */
public class App {
    public static void main(String args[]) {
        int option;

        do {
            option = menu();

            if(option > 0 && option < 7) {
                algorithm(option, input());
            } else if(option > 7) {
                System.out.println("OPCIÓN INVÁLIDA\n");
            }
        } while(option != 7);
    }
    
    static int menu() {
        Scanner read = new Scanner(System.in);
        int option;

        System.out.print("\nMENÚ\n 1. FCFS\n 2. SSTF\n 3. SCAN\n 4. C-SCAN\n "
                + "5. LOOK\n 6. C-LOOK\n 7. Salir" +
                "\nTeclee el número del algoritmo: ");
        option = validateInt(read.next());

        return option;
    }
    /**
     * This method uses the algorithm typed by the user
     * @param option represents the algorithm to execute
     * @param cylinders ArrayList of the cylinders
     */
    static void algorithm(int option, ArrayList<Integer> cylinders) {
        int initialCyl = cylinders.get(0);
        cylinders.remove(0);
        
        System.out.print("\nTABLA\n");
        switch (option) {
            case 1:
                FCFS fc = new FCFS(cylinders, initialCyl);
                fc.calculate();
                break;
            case 2:
                SSTF sstf = new SSTF(cylinders, initialCyl);
                sstf.calculate();
                break;
            case 3:
                SCAN sc = new SCAN(cylinders, initialCyl);
                sc.calculate();
                break;
            case 4:
                CSCAN cs = new CSCAN(cylinders, initialCyl);
                cs.calculate();
                break;
            case 5:
                LOOK lk = new LOOK(cylinders, initialCyl);
                lk.calculate();
                break;
            case 6:
                CLOOK cl = new CLOOK(cylinders, initialCyl);
                cl.calculate();
                break;
        }
    }
    
    /**
     * This method allows to the user input the cylinders to use
     * @return ArrayList with all cylinders to use in the algorithm
     */
    static ArrayList<Integer> input() {
        ArrayList<Integer> cylinders = new ArrayList<>();
        int requests;
        Scanner read = new Scanner(System.in);
        
        System.out.print("\nCILINDROS\nIngrese cilindros válidos "
                + "contenidos en el rango 0 a 199\nIngrese el número de peticiones: ");
        requests=validateInt(read.next());
        
        System.out.print("Cilindro inicial: ");
        cylinders.add(validate(validateInt(read.next()), cylinders));
        
        for(int i=1; i<=requests; i++){
            System.out.print("Ingrese el valor del cilindro " + i + ": ");
            cylinders.add(validate(validateInt(read.next()), cylinders));
        }
        
        return cylinders;
    }
    
    /**
     * This method validates if the value of the cylinders if correct
     * @param value proportionate by the user
     * @param cylinders ArrayList of the actual cylinders
     * @return the cylinder to add in the ArrayList
     */
    static int validate(int value, ArrayList<Integer>cylinders) {
        boolean exist = exist(value, cylinders); // Does the cylinder exist?
        
        if(value >= 0 && value <= 199 && !exist) {
            return value;
        } else {
            int cylinder;
            Scanner read = new Scanner(System.in);
            
            do {                
                System.out.print("\nCILINDRO NO VÁLIDO\nIngrese un cilindro"
                        + " válido o no tecleado anteriormente: ");
                cylinder = validateInt(read.next());
                
                exist = exist(cylinder, cylinders); // Does the cylinder exist?
            } while (cylinder < 0 || cylinder > 199 || exist);
            
            return cylinder;
        }
    }
    
    /**
     * Check if exists the typed value by the user in the actual ArrayList
     * @param value proportionate by the user
     * @param cylinders ArrayList of the actual cylinders
     * @return true if match the cylinder typed
     */
    static boolean exist(int value, ArrayList<Integer> cylinders) {
        return cylinders.indexOf(value) != -1;
    }
    
    static int validateInt(String a){
        try{
            return Integer.parseInt(a);                
        }catch(NumberFormatException e){
            System.err.println("Opción no valida");
        }    
        return -1;
    }
}
