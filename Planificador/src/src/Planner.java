package src;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Parent class
 * @author Carlos Acuña
 * @version 1.2
 */
public abstract class Planner {
    private ArrayList<Integer> cylinders;
    private int initialCyl;
    private int diskSize;
    
    public Planner(ArrayList<Integer> cylinders, int initialCyl) {
        this.cylinders = cylinders;
        this.initialCyl = initialCyl;
        diskSize = 200;
    }

    /**
     * Sort the data provided in ascending order
     */
    public void sortCylinders() {
        Collections.sort(cylinders);
    }
    
    /**
     * Returns the index of the first cylinder under or over the initial Cylinder
     * if theres no cylinder under returns -1, otherwise if there is no cylinder
     * over returns the array size
     * @param up indicates direction
     */
    public int firstClosestCyl(boolean up){            
        int i;
        if(cylinders.get(cylinders.size()-1) < initialCyl){
            if(up)              
                i = cylinders.size(); //goes to 199
            else
                i = cylinders.size()-1;                                      
        }else if(cylinders.get(0) > initialCyl){
            if(!up)
                i = -1; //goes to 0  
            else
                i = 0;                                      
        }
        else{
            int max, min; //helps indicate in wich rank we should search
            max = cylinders.size()-1;
            min = 0;
            i = cylinders.size()/2;
            while(!(cylinders.get(i) < initialCyl && cylinders.get(i+1) > initialCyl)){        
                //System.out.println(i); debbuging println
                if(initialCyl < cylinders.get(i)){
                    //decrement
                    max = i;                                 
                    i = (max + min) /2;
                }else{
                    min = i;                 
                    i = (max + min) /2;
                }
            }      
            if(up)
                i=i+1;
        }                        
        return i;
    }

    /**
     * Add a cylinder in the cylinders list
     * @param cylinder represents the element that will be added in the cylinders list.
     */
    public void addCylinder(int cylinder) {
        cylinders.add(cylinder);
    }

    /**
     * Remove a cylinder in the cylinders list.
     * @param cylinder represents the element that will be removed in the cylinders list.
     */
    public void removeCylinder(int cylinder) {
        cylinders.remove(cylinders.indexOf(cylinder));
    }

    /**
     * This method is used to calculate the correspond planning
     */
    abstract void calculate();

    /**
     * Calculate and return the displacement
     * @param currentCyl Current cylinder
     * @param requestedCyl Requested cylinder
     * @return Subtraction result between requestedCyl and currentCyl
     */
    public int getDisplacement(int currentCyl, int requestedCyl) {
        return Math.abs(requestedCyl - currentCyl);
    }

    /**
     * Calculate the average waiting time
     * @param waitTime Wait time
     * @return The avg between waitTime and data size
     */
    public float avgWaitingTime(int waitTime) {
        return (float) waitTime / cylinders.size();
    }

    public ArrayList<Integer> getCylinders() {
        return cylinders;
    }

    public void setCylinders(ArrayList<Integer> cylinders) {
        this.cylinders = cylinders;
    }

    public int getInitialCyl() {
        return initialCyl;
    }

    public void setInitialCyl(int initialCyl) {
        this.initialCyl = initialCyl;
    }
    
    public int getDiskSize() {
        return diskSize;
    }
}
