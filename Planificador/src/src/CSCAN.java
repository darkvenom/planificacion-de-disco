package src;

import java.util.ArrayList;

/**
 * This class calculate the C-SCAN algorithm
 * @author Mon
 * @version 1.1
 */
public class CSCAN extends Planner{
    public CSCAN(ArrayList<Integer> cylinders, int initialCyl) {
      super(cylinders, initialCyl);
   }
    
    @Override
    void calculate() {        
        ArrayList <Integer> cylinders = getCylinders();
        int currentCyl = getInitialCyl();
        int displacement, 
            waitTime = 0, 
            sumWaitingTime = 0;
        
        sortCylinders();
        
        boolean up = false;
        int start = firstClosestCyl(up), 
            n = start, auxCyl, 
            count = 0;
        
        System.out.printf("%-10s | %-10s | %-10s | %-10s\n", "Cilindro actual", "Cilindro solicitado",
                "Tiempo de espera", "Desplazamiento");
        
        for (int i = 0; i < cylinders.size() + 1; i++) {
            if (n<0) {
                auxCyl = 0;
                i --;
                up = !up;
                n = start;
                count++;
            } else if (count == 1) {
                auxCyl = getDiskSize()-1;
                up = false;
                count++;
                n = cylinders.size() -1;
            }else if(count == 2){
                auxCyl = cylinders.get(cylinders.size()-1);
                up = false;
                count=0;
                n = cylinders.size() -1;
            }else{
                auxCyl = cylinders.get(n);
            }
                
            displacement = getDisplacement(currentCyl, auxCyl);

            if (displacement != 0) {
                
                System.out.printf("%-15s | %-19s | %-16s | %-15s\n", currentCyl, auxCyl, waitTime, displacement);

                sumWaitingTime += waitTime;
                waitTime += displacement;
                currentCyl = auxCyl;
            }
            
            if(up)
                n++;
            else
                n--;
            
        }
        
        System.out.println("\nResultados finales\nDesplazamiento: " + waitTime +
             "\nTPE: " + avgWaitingTime(sumWaitingTime));
    }
}